# manpages-fr_en_cours

Le dépôt de ce projet n'a d'autre ambition que d'accueillir les
fichiers sur lesquels je travaille actuellement : il est destiné à
éviter de faire transiter sur la liste des fichiers au poids trop
important. Après le DONE et leur versement sur manpages-l10/po/fr/, ils
sont supprimés.

Il contient pour chaque fichier le fichier d'origine renommé
XXX.X.origine.po ou XXX.X.v0.po(par exemple, bash.1.origine.po) et le
fichier en cours de révision (xxxx.1.po).
